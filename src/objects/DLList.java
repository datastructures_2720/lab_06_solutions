package objects;

import listinterface.IList;
/**
 * This class should implement 'IList<DLine>'
 * 
 * @author Azim Ahmadzadeh [https://grid.cs.gsu.edu/~aahmadzadeh1/]
 *
 */
public class DLList implements IList<DLine> {

	/** The head (i.e., first element) of the list */
	private DLine head;
	/** The number of elements in the list */
	private int n;

	public DLList() {
		this.head = null;
		this.n = 0;
	}

	@Override
	public DLine get(int index) {
		
		// if index is not invalid.
		if(index >= this.n || index < 0)
			return null;
		
		DLine currentDLine = this.head;
		
		for(int i = 0; i < index; i++) {
			currentDLine = currentDLine.getNext();
		}

		return currentDLine;
	}
	
	@Override
	public void add(DLine dl) {

		// If the list is empty, add this as the first element (head)
		if (this.n == 0) {
			this.head = new DLine(dl);
		}
		// otherwise, add it to the end of the list
		else {
			DLine lastElement = this.get(this.n - 1);
			lastElement.setNext(new DLine(dl));
		}
		// increment the size of the list
		this.n++;
	}

	@Override
	public int add(DLine dl, int index) {

		if (index < 0 || index >= this.n)
			return -1;

		DLine newElement = new DLine(dl);
		// If it has to be added as the first element
		if (index == 0) {
			newElement.setNext(this.head);
			this.head = newElement;
		}
		// If it has to be added somewhere else
		else {
			DLine thisElement = this.get(index);
			DLine prevElement = this.get(index - 1);

			newElement.setNext(thisElement);
			prevElement.setNext(newElement);
		}
		// increment the size of the list
		this.n++;
		return 1;
	}


	@Override
	public int remove(int index) {
		
		// if index is not valid
		if(index >= this.n || index < 0)
			return -1;
		
		DLine toBeRemoved = this.get(index);
		
		// special case: removing the first element
		if(index == 0) {
			this.head = toBeRemoved.getNext();
			toBeRemoved.setNext(null);
			this.n--;
			return 0;
		}
		
		// general case: if this is NOT the first element
		DLine prevDLine = this.get(index-1);
		prevDLine.setNext(toBeRemoved.getNext());
		toBeRemoved.setNext(null);
		
		this.n--;
		return 0;

	}

	@Override
	public void empty() {
		this.head = null;
		this.n = 0;
	}

	@Override
	public int size() {
		return this.n;
	}

}
