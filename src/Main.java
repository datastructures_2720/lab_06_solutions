import listinterface.IList;
import objects.DLList;
import objects.DLine;

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * -> Class: Data Structures - 2720 - - - - - - - - - - - - - - - - - - - - - -
 * -> LAB: 06 [Solutions] - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Date: Friday 28 Sep, 2018 - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Subject: LinkedList - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Lab Web-page: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * [https://sites.google.com/view/azimahmadzadeh/teaching/data-structures-2720]
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * 
 * @author Azim Ahmadzadeh [https://grid.cs.gsu.edu/~aahmadzadeh1/] - - - - -
 */
public class Main {

	public static void main(String[] args) {

		/*-======================= S-1 ======================== *
		 * Test: size()											*
		 * -Create a list ('myList') and check its size.		*
		 * ======================= S-1 ======================== */startSection(1);
		IList<DLine> myList = new DLList();
		System.out.println("The size of an empty list: " + myList.size());

		/*-======================= S-2 ======================== *
		 * Test: add(int), size()								*
		 * -Add 5 elements to 'myList' using your add method.	*
		 * (elements: DLines of size {0,1,2,3,4}.				*
		 * -Check the size of the list. Is it 5?				*
		 * -Print your list using displayList and verify it.	*
		 * ======================= S-2 ======================== */startSection(2);
		myList.add(new DLine(0));
		myList.add(new DLine(1));
		myList.add(new DLine(2));
		myList.add(new DLine(3));
		myList.add(new DLine(4));
		myList.displayList();

		/*-======================= S-3 ======================== *
		 * Test: get(int)										*
		 * - Get the [first], [3rd] and [last] elements.		*
		 * - Print each DLine and verify your get method.		*
		 * - Get the 100th element! What do you get?			*
		 * ======================= S-3 ======================== */startSection(3);
		DLine tmpDLine = myList.get(0);
		System.out.println("index = 0 --> " + tmpDLine.length);
		tmpDLine = myList.get(3);
		System.out.println("index = 3 --> " + tmpDLine.length);
		tmpDLine = myList.get(myList.size() - 1);
		System.out.println("index = n-1 --> " + tmpDLine.length);
		tmpDLine = myList.get(100);
		System.out.println("Is NULL returned?  " + (tmpDLine == null));

		/*-======================= S-4 ======================== *
		 * Test: add(DLine, int)								*
		 * - Add a new DLine (size 11) as the [first] element.	*
		 * - Display your list and verify it.					*
		 * - Add a new DLine (size 22) as the [2nd] element.	*
		 * - Display your list and verify it.					*
		 * - Add a new DLine (size 33) as the [last] element.	*
		 * - Display your list and verify it.					*
		 * - Add a new DLine (size 44) as the [100th] element.	*
		 * - What is returned? Is that expected? Is your list	*
		 * affected?											*
		 * ======================= S-4 ======================== */startSection(4);
		tmpDLine = new DLine(11);
		myList.add(tmpDLine, 0);
		myList.displayList();
		// Add a new DLine as the 3rd element
		tmpDLine = new DLine(22);
		myList.add(tmpDLine, 2);
		myList.displayList();
		// Add a new DLine as the last element
		tmpDLine = new DLine(33);
		myList.add(tmpDLine, myList.size() - 1);
		myList.displayList();
		// Check if you can add an element at index = myList.size()
		tmpDLine = new DLine(44);
		int res = myList.add(tmpDLine, myList.size());
		System.out.println("Adding an element at an inappropriate position:  " + res + ".");
		myList.displayList();

		/*-======================= S-5 ======================== *
		 * Test: remove(int)									*
		 * - Remove the DLine at index 0.						*
		 * - Display your list and verify it.					*
		 * - Remove the DLine at index 2.						*
		 * - Display your list and verify it.					*
		 * - Remove the DLine at index n-1.						*
		 * - Display your list and verify it.					*
		 * - Remove the DLine at index 100!						*
		 * - What is returned? Is that expected? Is your list	*
		 * affected?											*
		 * ======================= S-5 ======================== */startSection(5);
		myList.remove(0);
		myList.displayList();
		myList.remove(2);
		myList.displayList();
		myList.remove(myList.size() - 1);
		myList.displayList();
		res = myList.remove(100);
		System.out.println("Removing an element that does not exist: " + res + ".");
		myList.displayList();

		/*-======================= S-6 ======================== *
		 * Test: remove(int)									*
		 * - Empty your entire list using the method 'empty'.	*
		 * - Display the list, and print out the size. Correct?	*
		 * ======================= S-6 ======================== */startSection(6);
		 myList.empty();
		 System.out.println("List after emptying:");
		 myList.displayList();
		 System.out.println("Size after emptying: " + myList.size());
	}

	/** IGNORE THIS METHOD **/
	private static void startSection(int i) {
		System.out.print("\n:::::::::::::::::::::::");
		System.out.print(" START [" + i + "] ");
		System.out.print(":::::::::::::::::::::::\n\n");
	}

	/** IGNORE THIS METHOD **/
	private static void endSection(int i) {
		System.out.print("\n________________________");
		System.out.print(" END [" + i + "] ");
		System.out.print("________________________\n\n");
	}

}
